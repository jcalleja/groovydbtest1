DROP TYPE IF EXISTS user_status CASCADE;
DROP TABLE IF EXISTS todouser CASCADE;

CREATE TYPE user_status AS ENUM('REGISTERED', 'TO_CONFIRM', 'NOT_REGISTERED');
CREATE TABLE todouser
(
  id SERIAL NOT NULL,
  email character varying(80),
  password character varying(200),
  status user_status,
  confirmationUrl character varying(280),
  CONSTRAINT todouser_pkey PRIMARY KEY (id)
);
;