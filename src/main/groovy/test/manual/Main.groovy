package test.manual

import groovy.sql.Sql

import javax.sql.DataSource

import org.springframework.context.support.GenericXmlApplicationContext

class Main {

	static main(args) {
		GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
		ctx.load("classpath:testContext.xml");
		ctx.refresh();

		DataSource dataSource = ctx.getBean("dataSource", javax.sql.DataSource.class);
		Sql sql = new Sql(dataSource)
		
		sql.eachRow("SELECT * FROM todouser") { user ->
			println user.email
			println user.password
			println user.status
			println user.confirmationUrl
			println '-' * 20
		}
	}
}
